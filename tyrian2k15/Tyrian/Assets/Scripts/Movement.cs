﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {

	private Vector3 mousePosition;
	public float moveSpeed = 0.1f;
	public Camera cam;
	// Use this for initialization
	void Start () {
		Screen.showCursor = false;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 pos = Input.mousePosition;
		pos.z = transform.position.z - cam.transform.position.z;
		transform.position = cam.ScreenToWorldPoint(pos);
	}
}
