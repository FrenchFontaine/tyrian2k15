﻿using UnityEngine;
using System.Collections;

public class BoundriesController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter (Collider myTrigger) {
		if(myTrigger.gameObject.name.ToLower().Equals("magnet")){
			myTrigger.gameObject.GetComponent<Magnet>().setActive(1);
			Debug.Log("Magnet entered trigger!");
		}
	}

	void OnTriggerExit (Collider myTrigger) {
		if(myTrigger.gameObject.name.ToLower().Equals("magnet")){
			myTrigger.gameObject.GetComponent<Magnet>().setActive(-1);
			Debug.Log("Magnet exited trigger!");
		}
	}
}
