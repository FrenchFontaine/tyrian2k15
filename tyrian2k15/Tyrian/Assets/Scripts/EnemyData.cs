﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Enemy data stores all the information needed to create a certain enenmy group, constructed of certian enemies, with certain patterns.
/// The purpose of this class is so many instances of EnemyData can be created and stored in a stack where another controller class can then take out each data structure
/// one by one to create the enemies and put them into the world
/// </summary>
public class EnemyData : MonoBehaviour {

	private string patternType;
	private int unitMovement;
	private string enemyType;
	private int numberOfEnemies;
	private Transform spawnOrigin;
	private GameObject enemyToSpawn;
	private Camera cam;
	private string direction;

	/// <summary>
	/// Initializes a new instance of the <see cref="EnemyData"/> class.
	/// </summary>
	/// <param name="patternType">Pattern type.</param>
	/// <param name="unitMovement">Unit movement.</param>
	/// <param name="enemyType">Enemy type.</param>
	/// <param name="numberOfEnemies">Number of enemies.</param>
	/// <param name="spawnOrigin">Spawn origin.</param>
	/// <param name="enemyToSpawn">Enemy to spawn.</param>
	/// <param name="cam">Cam.</param>
	/// <param name="direction">Direction.</param>

	public EnemyData(string patternType, int unitMovement, string enemyType, int numberOfEnemies, Transform spawnOrigin, GameObject enemyToSpawn, Camera cam, string direction){
		this.patternType = patternType;
		this.unitMovement = unitMovement;
		this.enemyType = enemyType;
		this.numberOfEnemies = numberOfEnemies;	
		this.spawnOrigin = spawnOrigin;
		this.enemyToSpawn = enemyToSpawn;
		this.cam = cam;
		this.direction = direction;
	}

	/// <summary>
	/// Sets the type of the pattern.
	/// </summary>
	/// <param name="patternType">Pattern type.</param>
	public void setPatternType(string patternType){
		this.patternType = patternType;
	}

	/// <summary>
	/// Sets the unit movement.
	/// </summary>
	/// <param name="unitMovement">Unit movement.</param>
	public void setUnitMovement(int unitMovement){
		this.unitMovement = unitMovement;
	}

	/// <summary>
	/// Sets the type of the enemy.
	/// </summary>
	/// <param name="enemyType">Enemy type.</param>
	public void setEnemyType(string enemyType){
		this.enemyType = enemyType;
	}

	/// <summary>
	/// Sets the number of enemies.
	/// </summary>
	/// <param name="numberOfEnemies">Number of enemies.</param>
	public void setNumberOfEnemies(int numberOfEnemies){
		this.numberOfEnemies = numberOfEnemies;	
	}

	/// <summary>
	/// Sets the spawn origin.
	/// </summary>
	/// <param name="spawnOrigin">Spawn origin.</param>
	public void setSpawnOrigin(Transform spawnOrigin){
		this.spawnOrigin = spawnOrigin;
	}

	/// <summary>
	/// Sets the enemy to spawn.
	/// </summary>
	/// <param name="enemyToSpawn">Enemy to spawn.</param>
	public void setEnemyToSpawn(GameObject enemyToSpawn){
		this.enemyToSpawn = enemyToSpawn;
	}

	/// <summary>
	/// Sets the cam.
	/// </summary>
	/// <param name="cam">Cam.</param>
	public void setCam(Camera cam){
		this.cam = cam;
	}

	/// <summary>
	/// Sets the direction.
	/// </summary>
	/// <param name="direction">Direction.</param>
	public void setDirection(string direction){
		this.direction = direction;
	}

	/// <summary>
	/// Gets the type of the pattern.
	/// </summary>
	/// <returns>The pattern type.</returns>
	public string getPatternType(){
		return patternType;
	}

	/// <summary>
	/// Gets the unit movement.
	/// </summary>
	/// <returns>The unit movement.</returns>
	public int getUnitMovement(){
		return unitMovement;
	}

	/// <summary>
	/// Gets the type of the enemy.
	/// </summary>
	/// <returns>The enemy type.</returns>
	public string getEnemyType(){
		return enemyType;
	}

	/// <summary>
	/// Gets the number of enemies.
	/// </summary>
	/// <returns>The number of enemies.</returns>
	public int getNumberOfEnemies(){
		return numberOfEnemies;
	}

	/// <summary>
	/// Gets the spawn origin.
	/// </summary>
	/// <returns>The spawn origin.</returns>
	public Transform getSpawnOrigin(){
		return spawnOrigin;
	}

	/// <summary>
	/// Gets the enemy to spawn.
	/// </summary>
	/// <returns>The enemy to spawn.</returns>
	public GameObject getEnemyToSpawn(){
		return enemyToSpawn;
	}

	/// <summary>
	/// Gets the cam.
	/// </summary>
	/// <returns>The cam.</returns>
	public Camera getCam(){
		return cam;
	}

	/// <summary>
	/// Gets the direction.
	/// </summary>
	/// <returns>The direction.</returns>
	public string getDirection(){
		return direction;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
