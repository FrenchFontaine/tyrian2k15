﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This class creates itself as a specific pattern given a pattern type, and number of enemies
/// </summary>
public class EnemyPatterns : MonoBehaviour {

	private string type;
	private ArrayList enemies;


	/// <summary>
	/// Initializes a new instance of the <see cref="EnemyPatterns"/> class.
	/// </summary>
	/// <param name="type">Type.</param>
	public EnemyPatterns(string type, ArrayList enemies){
		this.type = type;
		this.enemies = enemies;
		createPattern ();
	}


	private void createPattern(){
		switch (type.ToLower()) {
		case "verticalstraight":
			int origin = 1;
			float newY = 0.0f;
			foreach(Enemy e in enemies){
				//the first time through, the enemy will remain at the origin
				if(origin > 0){
					newY = e.transform.position.y;
					origin = -1;
				}else{//otherwise, all subsequest enemies will be moved up 2.0 units in the Y direction
					e.transform.position = new Vector3(e.transform.position.x, newY, e.transform.position.z);
				}
				Debug.Log("Setting enemy position at "+e.transform.position);
				newY+=0.3f;
			}
			break;
		default:
			Debug.LogError("ERROR: " + type.ToLower() + " did not match any pattern types!!");
			break;
		}

	}

	public string getPatternType(){
		return type;
	}
	
	// Use this for initialization
	void Start () {

	}

	private void waitBreak(){
		//do nothing
	}
	
	// Update is called once per frame
	void Update () {

	}
}
