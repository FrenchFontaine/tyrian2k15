﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// This class is composed of 1 to many abstract Enemy objects ALL of the same type.
/// A new instace of this class will be created for each Enemy type
/// </summary>
public class EnemyGroup : MonoBehaviour {

	//variables
/******************************************************************************/
	private int unitMovement;

	//GameObject ArrayList is really dumb...but Generics isn't working right now...just fuck it for now i'lll fix later
	private ArrayList enemies;

	private string enemyGroupType;
<<<<<<< HEAD
	private string direction;
=======
>>>>>>> e503ccde60498ca6ad7609b7a5bd917cf9295aeb
	
	private EnemyPatterns pattern;

	private Transform spawnOrigin;

	private GameObject enemyToSpawn;

	private int active;

	private Camera cam;
/******************************************************************************/



	/// <summary>
	/// Initializes a new instance of the <see cref="EnemyGroup"/> class.
	/// </summary>
	/// <param name="patternType">Pattern type.</param>
	/// <param name="unitMovement">Unit movement.</param>
	/// <param name="enemyType">Enemy type.</param>
	/// <param name="numberOfEnemies">Number of enemies.</param>
<<<<<<< HEAD
	public EnemyGroup(string patternType, int unitMovement, string enemyType, int numberOfEnemies, Transform spawnOrigin, GameObject enemyToSpawn, Camera cam, string direction){
=======
	public EnemyGroup(string patternType, int unitMovement, string enemyType, int numberOfEnemies, Transform spawnOrigin, GameObject enemyToSpawn, Camera cam){
>>>>>>> e503ccde60498ca6ad7609b7a5bd917cf9295aeb
		this.spawnOrigin = spawnOrigin;
		if (spawnOrigin == null) {
			Debug.LogError ("no MagnetSpawn detected");	
		}
		print (this.cam == null);
		this.cam = cam;
		print (this.cam == null);
		this.enemyToSpawn = enemyToSpawn;
<<<<<<< HEAD
		this.direction = direction;
=======
>>>>>>> e503ccde60498ca6ad7609b7a5bd917cf9295aeb
		enemyGroupType = enemyType;
		enemies = createEnemyCollection (enemyType, numberOfEnemies);
		pattern = new EnemyPatterns (patternType, enemies);
		active = -1;
		this.unitMovement = unitMovement;
		Start ();
		Update ();
	}

	/// <summary>
	/// Creates the collection of given number of given enemy type.
	/// </summary>
	/// <returns>The enemy collection.</returns>
	/// <param name="enemyType">Enemy type.</param>
	/// <param name="numberOfEnemies">Number of enemies.</param>
	private ArrayList createEnemyCollection(string enemyType, int numberOfEnemies){
		int i;
		ArrayList enemyCollection = new ArrayList ();
		switch (enemyType.ToLower()) {
		case "magnet":
			for(i = 0; i < numberOfEnemies; ++i){
				GameObject magnetObject =  (GameObject)(Instantiate (enemyToSpawn, spawnOrigin.position, spawnOrigin.rotation));
				Magnet newMagnet = magnetObject.GetComponent<Magnet>();
<<<<<<< HEAD
				newMagnet.setSpeed(0.01f);
=======
				newMagnet.setSpeed(2.0f);
>>>>>>> e503ccde60498ca6ad7609b7a5bd917cf9295aeb
				newMagnet.setHealth(100.0f);
				newMagnet.setFireRate(3.0f);
				newMagnet.setBoss(-1);
				newMagnet.setCamera(cam);
<<<<<<< HEAD
				newMagnet.setFirstPass(1);
				newMagnet.setWaitForSeconds(-1);
				if(direction.ToLower().Equals("right")){
					newMagnet.setMoveRight(1);
				}else if(direction.ToLower().Equals("left")){
					newMagnet.setMoveRight(-1);
				}else{
					print("ERROR: direction " + direction + " did not match a valid direction");
				}
				/*If somehow we go back to splines use this*///newMagnet.GetComponent<SplineController> ().setSplineRoot (GameObject.Find("MagnetSplineRootOne"));
=======
>>>>>>> e503ccde60498ca6ad7609b7a5bd917cf9295aeb
				Debug.Log("adding magnet to enemyCollection");
				enemyCollection.Add (newMagnet);//eeeeh....won't hard code in params but for now...call the cops...i don't give a fuck
			}
			break;
		case "robot":
			for(i = 0; i < numberOfEnemies; ++i){
				enemyCollection.Add (new Robot());
			}
			break;
		case "compass":
			for(i = 0; i < numberOfEnemies; ++i){
				enemyCollection.Add (new Compass());
			}
			break;
		case "purpledragon":
			for(i = 0; i < numberOfEnemies; ++i){
				enemyCollection.Add (new PurpleDragon());
			}
			break;
		case "wheatly":
			for(i = 0; i < numberOfEnemies; ++i){
				enemyCollection.Add (new Wheatly());
			}
			break;
		case "crabboss":
			for(i = 0; i < numberOfEnemies; ++i){
				enemyCollection.Add (new CrabBoss());
			}
			break;
		default:
			Debug.LogError("ERROR: " + enemyType.ToLower() + " did not match any enemy types to create!");
			break;
		}

		return enemyCollection;
	}
	

	// Use this for initialization
	void Start () {
<<<<<<< HEAD

=======
		/*Debug.Log ("waiting");
		int count = 0;
		while(count < 5000){
			Debug.Log(count);
			count++;
		}
		Debug.Log ("done waiting");*/
		//active = 1;
>>>>>>> e503ccde60498ca6ad7609b7a5bd917cf9295aeb
	}
	
	// Update is called once per frame
	void Update () {
		Debug.Log ("updating");
		//active > 0 = true else false
		if(active < 0){
			switch(pattern.getPatternType().ToLower()){
			case "verticalstraight":
				if(enemyGroupType.ToLower().Equals("magnet")){
					foreach(Magnet go in enemies){
						Magnet mag = go.GetComponent<Magnet>();
						mag.beginMovement(1);
					}
				}
				break;
			}
			active = 1;
		}
	}
}
