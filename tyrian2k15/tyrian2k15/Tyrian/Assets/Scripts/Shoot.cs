﻿using UnityEngine;
using System.Collections;

public class Shoot : MonoBehaviour {

	//0 is single burst
	public float fireRate = 0f;
	public float damage = 10f;
	public LayerMask notToHit;
	//determines whihc gun should be fired to make a "staggered" shooting pattern. 1=left gun -1=right gun
	private int gunSide = 0;
	private float timeToFire = 0f;
	public int altGuns;

	Transform frontLeftFirePoint;
	Transform frontRightFirePoint;
	Transform frontFarLeftFirePoint;
	Transform frontFarRightFirePoint;

	Transform gameTopEdge;

	public GameObject projectile;

	// Use this for initialization
	void Start () {
		gunSide = 1;
		frontLeftFirePoint = transform.FindChild ("FrontLeftFirePoint");
		if (frontLeftFirePoint == null) {
			Debug.LogError("no Front Left Fire Point child detected");		
		}
		frontRightFirePoint = transform.FindChild ("FrontRightFirePoint");
		if (frontRightFirePoint == null) {
			Debug.LogError("no Front Right Fire Point child detected");		
		}
		frontFarLeftFirePoint = transform.FindChild ("FrontFarLeftFirePoint");
		if (frontFarLeftFirePoint == null) {
			Debug.LogError("no Front Far Left Fire Point child detected");		
		}
		frontFarRightFirePoint = transform.FindChild ("FrontFarRightFirePoint");
		if (frontFarRightFirePoint == null) {
			Debug.LogError("no Front Far Right Fire Point child detected");		
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetButton("Fire1") && Time.time > timeToFire){
			timeToFire = Time.time + (1/fireRate);
			Debug.Log ("Time = " + Time.time + "\ntimeToFire = " + timeToFire);
			shoot(altGuns);
		}
	}

	//center of two objects, not really needed ATM
	private float getObjectBoundries(GameObject obj1){
		float centerDiff = (transform.position - obj1.transform.position).magnitude; // distance between 2 objects
		return centerDiff;
	}

	//fuckit , if we can go the easy route I say why not
	private void shoot(int alternate){
		//use alternating guns
		if (alternate > 0) {
						if (gunSide > 0) {
								Instantiate (projectile, frontLeftFirePoint.position, transform.rotation);
						} else if (gunSide < 0) {
								Instantiate (projectile, frontRightFirePoint.position, transform.rotation);
						}
						//This alternates the side of which gun should fire
						gunSide *= -1;
		} else {//do not use alternating guns
			Instantiate (projectile, frontLeftFirePoint.position, transform.rotation);
			Instantiate (projectile, frontRightFirePoint.position, transform.rotation);
		}

		//WAY better more solid way to fire and record projectiles, but not finished, harder, and fuck off this is early in the game LOL
		/*Vector2 mousePosition = new Vector2 (Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
		Debug.Log("Shooting");
		Vector2 firePointPosition = new Vector2 (frontFirePoint.position.x, frontFirePoint.position.y);
		RaycastHit2D hit = Physics2D.Raycast (firePointPosition, mousePosition-firePointPosition, 100, notToHit);

		Debug.DrawLine (frontFirePoint.position, Color.green);
		Debug.Log (transform.rotation);
		*/
		
	}
	
	private void wait(){}
	
}
