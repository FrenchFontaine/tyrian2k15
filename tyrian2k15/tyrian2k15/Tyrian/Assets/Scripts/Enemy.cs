﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Abstract class with base attributes all enemies share
/// Each single ENEMY will extend this class
/// </summary>
public abstract class Enemy : MonoBehaviour {

	//variables
/******************************************************************************/
	private float speed;
	private float health;
	private float fireRate;

	/*
	 * isBoss, isActive, isAlive > 0 then true. < 0 then false
	 */
	private int boss;
	//if true then this enemy can be interacted with. This ensures ALL emnemies don't die off screen if the player is just shooting constantly
	private int active; 
	//true upon creation. false when health <= 0.0
	private int alive;
	private int movementType;

	//Determines the type of enemy
	private string type;

	private Camera cam;
/******************************************************************************/




	// Use this for initialization
	void Start () {
		//really nothing here because there is a constructor
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	/// <summary>
	/// Attack is implemented by all extending enemyish classes since each enemy (mostly) has its own form of attack
	/// </summary>
	public abstract void attack ();

	/// <summary>
	/// Applies the damage.
	/// </summary>
	/// <returns>The damage.</returns>
	/// <param name="damage">Damage.</param>
	public abstract float applyDamage (float damage);

	/// <summary>
	/// Initializes a new instance of the <see cref="Enemy"/> class.
	/// </summary>
	public Enemy(){
		Debug.Log ("base enemy class");
		alive = 1;
	}
	
	/// <summary>
	/// Sets the speed.
	/// </summary>
	/// <param name="speed">Speed.</param>
	public void setSpeed(float speed){
		this.speed = speed;
	}

	/// <summary>
	/// Sets the camera.
	/// </summary>
	/// <param name="cam">Cam.</param>
	public void setCamera(Camera cam){
		this.cam = cam;
	}

	/// <summary>
	/// Gets the camera.
	/// </summary>
	/// <returns>The camera.</returns>
	public Camera getCamera(){
		return cam;
	}

	/// <summary>
	/// Sets the health.
	/// </summary>
	/// <param name="health">Health.</param>
	public void setHealth(float health){
		this.health = health;
	}

	/// <summary>
	/// Sets the fire rate.
	/// </summary>
	/// <param name="fireRate">Fire rate.</param>
	public void setFireRate(float fireRate){
		this.fireRate = fireRate;
	}

	/// <summary>
	/// Sets the boss.
	/// </summary>
	/// <param name="boss">Boss.</param>
	public void setBoss(int boss){
		this.boss = boss;
	}

	/// <summary>
	/// Sets the active.
	/// </summary>
	/// <param name="active">Active.</param>
	public void setActive(int active){
		this.active = active;
	}

	/// <summary>
	/// Sets the alive.
	/// </summary>
	/// <param name="alive">Alive.</param>
	public void setAlive(int alive){
		this.alive = alive;
	}


	/// <summary>
	/// Gets the speed.
	/// </summary>
	/// <returns>The speed.</returns>
	public float getSpeed(){
		return speed;
	}

	/// <summary>
	/// Gets the health.
	/// </summary>
	/// <returns>The health.</returns>
	public float getHealth(){
		return health;
	}

	/// <summary>
	/// Gets the fire rate.
	/// </summary>
	/// <returns>The fire rate.</returns>
	public float getFireRate(){
		return fireRate;
	}

	/// <summary>
	/// Determines if is the boss.
	/// </summary>
	/// <returns>boss.</returns>
	public int isBoss(){
		return boss;
	}

	/// <summary>
	/// Determines if is active.
	/// </summary>
	/// <returns>active.</returns>
	public int isActive(){
		return active;
	}

	/// <summary>
	/// Determines if is alive.
	/// </summary>
	/// <returns>alive.</returns>
	public int isAlive(){
		return alive;
	}

	public void setMovementType(int movementType){
		this.movementType = movementType;
	}

	public int getmovementType(){
		return movementType;
	}

	public Bounds OrthographicBounds(Camera camera)
	{
		Debug.Log (camera == null);
		float screenAspect = (float)Screen.width / (float)Screen.height;
		float cameraHeight = camera.orthographicSize * 2;
		Bounds bounds = new Bounds(
			camera.transform.position,
			new Vector3(cameraHeight * screenAspect, cameraHeight, 0));
		return bounds;
	}
	
}
