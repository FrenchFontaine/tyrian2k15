﻿using UnityEngine;
using System.Collections;

public class Robot : Enemy {
	
	public override void attack (){
		
	}

	/// <summary>
	/// Applies the damage.
	/// </summary>
	/// <returns>The damage.</returns>
	/// <param name="damage">Damage.</param>
	public override float applyDamage (float damage)
	{
		return 0.0f;
	}
	
	public Robot(){
		//base abstract class Enemy constructor always called first automagically
	}
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
