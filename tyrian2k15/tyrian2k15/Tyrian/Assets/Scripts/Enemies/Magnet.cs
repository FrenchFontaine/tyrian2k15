﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Magnet is a specific type of Enemy but is still an enemy thus, extends the Enemy class
/// </summary>
public class Magnet : Enemy {
	//with normal USP Talon bullets, I die in two hits. 


	/// <summary>
	/// Magnet jsut flys around hoping to collide with the target. lol. doesn't shoot a damn thing
	/// </summary>
	public override void attack (){
		//attack by simply being there.
	}

	/// <summary>
	/// Applies the damage.
	/// </summary>
	/// <returns>The damage.</returns>
	/// <param name="damage">Damage.</param>
	public override float applyDamage (float damage)
	{
		//if -1 was passed in that means the default amount of damage is done to this  enemey. In this case its -50% of health
		//otherwise, the provided damage is applied
		if (damage == -1.0f) {
			damage = 50.0f;		
		}
		float currentHealth = getHealth ();
		currentHealth -= damage;
		if (currentHealth > 0.0f) {
			setHealth(currentHealth);
			return getHealth ();
		} else {
			//if the health is <= 0.0 set alive=-1, health=0.0f, active=-1 then return -1 to indicate this enemy is dead
			setHealth(0.0f);
			setActive(-1);
			setAlive(-1);
			return -1;
		}
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="Magnet"/> class.
	/// </summary>
	/// <param name="speed">Speed.</param>
	/// <param name="health">Health.</param>
	/// <param name="fireRate">Fire rate.</param>
	/// <param name="boss">Boss.</param>
	public Magnet(float speed, float health, float fireRate, int boss, Camera cam){
		//base abstract class Enemy constructor always called first
		setSpeed (speed);
		setHealth (health);
		setFireRate (fireRate);
		setBoss (boss);
		setActive (-1);
		setCamera (cam);

	}


	public void beginMovement(int movementType){
		switch(movementType){
		case 1: 
			print("setting to one!!!");
			setMovementType(1);
			print ("movementtype: " + getmovementType());
			break;
		default:
			Debug.Log("movementType: " + movementType + " did not match any movement types");
			break;
		}
	}

	public void moveTypeOne(){
		Debug.Log ("moving enemy with movementTypeOne");
		//begin moving down until at the bottom of the screen
		float amountToMove = getSpeed() * Time.deltaTime;
		Debug.Log (Vector3.down);
		//transform.Translate (Vector3.down * amountToMove, Space.World);
		transform.Translate (new Vector3(0.0f, -0.01f, 0.0f), Space.World);
		Debug.Log ("world coordinates: "+OrthographicBounds(getCamera()).size.ToString() + "\nmagnet position: " + transform.position);
	}

	// Use this for initialization
	void Start () {
	
	}

	// Use this for initialization
	void Update () {
		Debug.Log ("updating");
		print ("movementtype: " + getmovementType());
		if(getmovementType() > 0){
			switch(getmovementType()){
			case 1:
				print ("moving type 1");
				moveTypeOne ();
				break;
			default:
				break;
			}
		}
	}

}
