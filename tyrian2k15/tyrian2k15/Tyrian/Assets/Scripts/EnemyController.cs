﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// This class is the head honcho. 
/// This controls what enemies are created, when, in what pattern, in what group, all that jazz
/// </summary>
public class EnemyController : MonoBehaviour {

	//later will have a collection of all enemies in here and then to create a new group of one, it will be pulled from the list. 
	public GameObject magnet;
	public GameObject wheatly;

	public Camera cam;

	private ArrayList currentEnemyGroups;
	public ArrayList spawnableEnemies;
	
	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start () {
		/*spawnableEnemies = new BigDictionary<string, GameObject> ();
		spawnableEnemies.Add (magnet);
		spawnableEnemies.Add (wheatly);
		foreach(GameObject go in spawnableEnemies){
			if(go.name.Equals("MagnetEnemy")){

			}
		}*/
		currentEnemyGroups = new ArrayList ();
		EnemyGroup currentGroup = new EnemyGroup ("VerticalStraight", 1, "magnet", 6, transform.FindChild ("MagnetSpawn"), magnet, cam);
		currentEnemyGroups.Add(currentGroup);
	}

	private void waitBreak(){
		//do nothing
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
