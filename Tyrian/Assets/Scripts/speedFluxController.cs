﻿using UnityEngine;
using System.Collections;

public class speedFluxController : MonoBehaviour {

	private Enemy_v2 enemy;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter (Collider myTrigger) {
		string colName = myTrigger.name;
		if (colName =="tempMagnetEnemy_v2") {
			(myTrigger.GetComponent<Enemy_v2> ()).beginSpeedFlux();
		}else if (myTrigger){

		}
	}

	void OnTriggerExit (Collider myTrigger) {
		string colName = myTrigger.name;
		if (colName =="tempMagnetEnemy_v2") {
			(myTrigger.GetComponent<Enemy_v2> ()).endSpeedFlux();
		}
	}

}
