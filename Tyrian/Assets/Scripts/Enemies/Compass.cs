using UnityEngine;
using System.Collections;

/// <summary>
/// Compass is a specific type of Enemy but is still an enemy thus, extends the Enemy class
/// </summary>
public class Compass : Enemy {
	
	/// <summary>
	/// Attack is implemented by all extending enemyish classes since each enemy (mostly) has its own form of attack
	/// </summary>
	public override void attack (){
		
	}

	/// <summary>
	/// Applies the damage.
	/// </summary>
	/// <returns>The damage.</returns>
	/// <param name="damage">Damage.</param>
	public override float applyDamage (float damage)
	{
		return 0.0f;
	}
	
	public Compass(){
		//base abstract class Enemy constructor always called first
	}
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		
	}
}