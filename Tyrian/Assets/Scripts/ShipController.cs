﻿using UnityEngine;
using System.Collections;

public class ShipController : MonoBehaviour {

	private int health;
	private int sheild;

	public int getHealth(){
		return health;
	}

	public int getSheild(){
		return sheild;
	}


	public bool hasSheild(){
		return getSheild () > 0;
	}

	public bool isAlive(){
		return getHealth () > 0;
	}
	
	// Use this for initialization
	//not sure if health and shield should be the same. OBV one regens one doesn't but whatever
	void Start () {
		health = 100;
		sheild = 100;
	}
	
	// Update is called once per frame
	void Update () {
		if (!isAlive ()) {
			print ("Game Over");
		}			
	}

	private void takeDamage(int system, int damageAmount){
		if (system > 0) {//sheild takes damage
			sheild -= damageAmount;
		} else {//take health damage
			health -= damageAmount;
		}
	}

	void OnCollisionEnter(Collision col){
		int system = 0;
		int damageAmt = 0;
		if (isAlive ()) {
						if (col.gameObject.name.Contains ("Enemy")) {//enemy ship has crashed into player
								if (hasSheild ()) {
										//shield has higher stopping power
										system = 1;
										damageAmt = 50;
								} else {
										//health without sheild takes more damage
										system = -1;
										damageAmt = 100;	
								}
						} else if (col.gameObject.name.Contains ("Projectile")) {//enemy bullet has hit player
								if (hasSheild ()) {
										//shield has higher stopping power
										system = 1;
										damageAmt = 10;
								} else {
										//health without sheild takes more damage
										system = -1;
										damageAmt = 15;	
								}
						}
						takeDamage (system, damageAmt);
				}
	}
}
