﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class EnemyGroup_v2 : MonoBehaviour {

	//variables
	/******************************************************************************/

	//Collection of enemies in this group
	private Enemy_v2[] enemies;
	//Pattern with which to assemble the enemt formation
	private EnemyPatterns pattern;
	//determines how long of a launch cycle to have
	private int launchCounter = -1;
	//determines the interval at which the launch should occur
	private int launchFrequency = 0;
	//necessary for accessing indecies
	private int i = 0;

	/******************************************************************************/
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		//only increment the launch counter if there is something that needs a launch countdown
		if (launchCounter >= 0) {
			//print ("launchCounter: " + launchCounter);
			launchCounter -= 1;
			//prints out every milisecond
		if (launchCounter % launchFrequency == 0) {
				//print ("LAUNCHING!");
				if(i < enemies.Length){
					enemies[i].launch();
					enemies[i].GetComponent<Rigidbody>().isKinematic = false;
					++i;
				}else{
					//print ("Launch procedure has been ended");
					endLaunchProcedure();
				}
			}
		}
	}

	//allows the launch counter to begin counting down from given frequency
	private void beginLaunchProcedure(int countdownStart, int launchFrequency){
		launchCounter = countdownStart;
		this.launchFrequency = launchFrequency;
	}

	private void endLaunchProcedure(){
		launchCounter = -1;
	}

	public void launchEnemies(){
		switch(pattern.getPatternType().ToLower()){
		case "verticalstraight":
			//for x miliseconds launch every interval y
			beginLaunchProcedure(pattern.getPatternLength(), pattern.getPatternFrequency());
				break;
		case "cyclicalleft":
			//for x miliseconds launch every interval y
			beginLaunchProcedure(pattern.getPatternLength(), pattern.getPatternFrequency());
			break;
		}
	}

	/// <summary>
	/// Creates enemyFrequency amount of enemyType from the ObjectPool, sets their position at spawnLoc, sets the individual enemyType spline and adds to an arrayList to be passed to pattern to create the pattern class.
	/// </summary>
	/// <param name="patternType">Pattern type.</param>
	/// <param name="unitMovement">Unit movement.</param>
	/// <param name="enemyType">Enemy type.</param>
	/// <param name="numberOfEnemies">Number of enemies.</param>
	/// //Spawn this enemy group 
	public void create(string enemyType, Transform spawnLoc, int enemyFrequency, Spline splinePath, string patternType, int hasSpeedFlux){
		//allocate enough space for exactly the amount of enemies needed
		enemies = new Enemy_v2[enemyFrequency];

		for(int i=0; i<enemyFrequency; ++i){
			//print("Pooling prefab and GameObject for later use: " + enemyType);
			Enemy_v2 e = (ObjectPool.instance.GetObjectForType (enemyType, true)).GetComponent<Enemy_v2>();
			e.transform.position = spawnLoc.position;
			e.setSpline(splinePath);
			e.setSplinePath();
			if(hasSpeedFlux > 0 && enemyType.Equals("tempMagnetEnemy_v2")){
				//print ("setting speed flux");
				e.setHasSpeedFlux(hasSpeedFlux);
				e.setSpeedFlux(23.5f, 0.1f, 23.5f, 0.2f, "z");
			}
			enemies[i] = e;
		}
		pattern = new EnemyPatterns (patternType, enemies);
	}

	public Enemy_v2[] getEnemies(){
		return enemies;
	}

}
