﻿using UnityEngine;
using System.Collections;

public class Enemy_v2 : MonoBehaviour {
	
	//variables
	/******************************************************************************/
	private float speed;
	private float health;
	private float fireRate;
	
	/*
	 * isBoss, isActive, isAlive > 0 then true. < 0 then false
	 */
	//if true then this enemy can be interacted with. This ensures ALL emnemies don't die off screen if the player is just shooting constantly
	private int active; 
	//true upon creation. false when health <= 0.0
	private int alive;
	private int movementType;
	private int firstPass; 
	private int moveRight;
	
	//Determines the type of enemy
	private string type;
	//determines which axis 
	private string axisOfFlux;

	private Spline spline;
	private SplineAnimator splineAnimator;
	private SplineNode finishNode;

	private float startThreshold;
	private float endThreshold;
	private float startThresholdSpeed;
	private float endThresholdSpeed;

	private int hasSpeedFlux;
	private int passedStartThreshhold;
	private int enteredGameSpace;

	private int check;

	
	/******************************************************************************/

	
	/// Raises the became invisible event.
	void OnBecameInvisible () {
		//Destroy (gameObject);
	}
	
	/// Attack is implemented by all extending enemyish classes since each enemy (mostly) has its own form of attack
	public void attack (){
		//nothing yet
	}
	
	/// Applies the damage.
	public float applyDamage (float damage){
		//still nothing yet
		return 100f;
	}
	
	/// Sets the speed.
	public void setSpeed(float speed){
		this.speed = speed;
	}
	
	/// Sets the health.
	public void setHealth(float health){
		this.health = health;
	}
	
	/// Sets the rate of fire.
	public void setFireRate(float fireRate){
		this.fireRate = fireRate;
	}

	
	/// Sets whether this enemy is active or not.
	public void setActive(int active){
		this.active = active;
	}
	
	/// Sets whether this enemy is alive or not....may not be necessary
	public void setAlive(int alive){
		this.alive = alive;
	}
	
	
	/// Gets the enemy speed.
	public float getSpeed(){
		return speed;
	}
	
	/// Gets the enemy health.
	public float getHealth(){
		return health;
	}
	
	/// Gets the fire rate.
	public float getFireRate(){
		return fireRate;
	}

	
	/// Determines if is active.
	public int isActive(){
		return active;
	}
	
	/// <summary>
	/// Determines if is alive.
	/// </summary>
	/// <returns>alive.</returns>
	public int isAlive(){
		return alive;
	}

	/// Sets if the enemy is on its first pass.
	public void setFirstPass(int firstPass){
		this.firstPass = firstPass;
	}

	/// gets if the enemy is on its first pass.
	public int getFirstPass(){
		return firstPass;
	}
	
	/// Sets the type of the movement.
	public void setMovementType(int movementType){
		this.movementType = movementType;
	}
	
	/// Sets the move right.
	public void setMoveRight(int moveRight){
		this.moveRight = moveRight;
	}

	// if > 0 then moving right 
	// if < 0 then moving left
	public int getMoveRight(){
		return this.moveRight;
	}
	
	/// Getmovements the type.
	public int getmovementType(){
		return movementType;
	}

	public void setSpline(Spline spline){
		this.spline = spline;
	}

	public void resetSpline(){
		GetComponent<SplineAnimator> ().setSpline (null);
		toggleSplineAnimator ();
	}

	//find and set the spline to be used for this enemy
	public void setSplinePath(){
		//print ("Spline name: "  +GetComponent<SplineAnimator> ().name);
		this.splineAnimator = GetComponent<SplineAnimator> ();
		GetComponent<SplineAnimator> ().setSpline (spline);
		splineAnimator.enabled = false;
	}

	public void toggleSplineAnimator(){
		if (splineAnimator.enabled) {
			splineAnimator.enabled = false;		
		} else {
			splineAnimator.enabled = true;		
		}
	}

	public SplineAnimator getSplineAnimator(){
		return splineAnimator;
	}

	public void launch (){
		toggleSplineAnimator ();
	}

	/// <summary>
	/// Sets the speed flux.
	/// </summary>
	/// <param name="startThresh">position at which to change speed.</param>
	/// <param name="startThreshSpeed">amount to change speed at start.</param>
	/// <param name="endThresh">position at which to change speed back.</param>
	/// <param name="endThreshSpeed">amount to change speed at end.</param>
	public void setSpeedFlux(float startThresh, float startThreshSpeed, float endThresh, float endThreshSpeed, string axis){
		this.startThreshold = startThresh;
		this.startThresholdSpeed = startThreshSpeed;
		this.endThreshold = endThresh;
		this.endThresholdSpeed = endThreshSpeed;
		this.axisOfFlux = axis;
	}

	public void modifySpeed(float travelSpeed){
		splineAnimator.setSpeed (travelSpeed);
	}

	public void setHasSpeedFlux(int speedFlux){
		hasSpeedFlux = speedFlux;
	}

	// Use this for initialization
	void Start () {
		//print ("Enemy_v2 script found");
		passedStartThreshhold = -1;
		enteredGameSpace = -1;
		check = 1;
	}

	void OnCollisionEnter(Collision col){
		if (col.gameObject.name.ToLower().Contains("ship")) {
			ObjectPool.instance.PoolObject(this.gameObject);
		}
	}

	public void beginSpeedFlux(){
		//this means the enemy movement has a certain time at whihc the speed needs to fluctuate
		if(hasSpeedFlux > 0){
			switch(axisOfFlux){
			case "x":
					//print ("x: passed start threshold");
					splineAnimator.setSpeed(startThresholdSpeed);
					passedStartThreshhold = 1;
				break;
			case "y":
					//print ("y: passed start threshold");
					splineAnimator.setSpeed(startThresholdSpeed);
					passedStartThreshhold = 1;
				break;
			case "z":
					//print ("z: passed start threshold");
					splineAnimator.setSpeed(startThresholdSpeed);
					passedStartThreshhold = 1;
				break;
			}
		}
	}

	public void endSpeedFlux(){
		//this means the enemy movement has a certain time at whihc the speed needs to fluctuate
		if(hasSpeedFlux > 0){
			switch(axisOfFlux){
			case "x":
					//print ("x: passed start threshold");
					splineAnimator.setSpeed(endThresholdSpeed);
					hasSpeedFlux = -1;
					check = -1;
				break;
			case "y":
					//print ("y: passed end threshold");
					splineAnimator.setSpeed(endThresholdSpeed);
					hasSpeedFlux = -1;
					check = -1;
				break;
			case "z":
					//print ("z: passed end threshold");
					splineAnimator.setSpeed(endThresholdSpeed);
					hasSpeedFlux = -1;
					check = -1;
				break;
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		//print ("relaying transform local position: " + transform.localPosition);
		//check if enemy has entered game view
	}
}
