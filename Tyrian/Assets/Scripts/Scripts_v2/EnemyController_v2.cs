﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyController_v2 : MonoBehaviour {

	public GameObject[] spawnLocations = new GameObject[4];
	public Queue<EnemyGroup_v2> enemyGroups = new Queue<EnemyGroup_v2>();
	public GameObject[] splinePaths = new GameObject[8];

	private int timer = 0;
	//determines if enemies should be created and ready for action
	private int shouldSpawn;
	//determines if enemies should actually enter the game
	private int shouldLaunch;
	//used for indicie accessing
	private int i = 0;
	private int counting;

	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start () {
		shouldSpawn = 1;
		shouldLaunch = -1;
		counting = 1;
	}
	
	// Update is called once per frame
	void Update () {
		if(counting > 0){
			//print ("timer = " + timer);
			if (timer > 5 && shouldSpawn>0) {//spawn 5 miliseconds into timer
				//print ("spawning");
				spawn ();	
				shouldSpawn = -1;
				shouldLaunch = 1;
			}else if(timer > 10 && shouldLaunch>0){//launch 10 miliseconds
				//print ("launching");
				launch();
				shouldLaunch = -1;
			}else if(timer == 500){//reset timer and spawn and launch variables 500 miliseconds in
				//print ("resetting");
				timer = 0;
				shouldSpawn = 1;
				shouldLaunch = 1;
				counting = 1;
			}
			timer += 1;
		}
	}

	private void launch(){
		if (enemyGroups.Count > 0) {
			//print ("launching enemies");
			enemyGroups.Dequeue().launchEnemies();
		} else {
			counting = -1;
			Debug.LogError("ERROR: enemyGroups is empty. Cannot launch");
		}

	}
	
	private void spawn(){
		if (1 == 1) {
						if (i <= 4) {
								//print ("spawning enemy");
								EnemyGroup_v2 enemyGroup = gameObject.AddComponent<EnemyGroup_v2> ();
								print ("spline = " + splinePaths[i].GetComponent<Spline>().name);
								if (i < 4) {
								print ("i = " + i);
										enemyGroup.create ("tempMagnetEnemy_v2", (spawnLocations [i]).transform, 6, (splinePaths [i]).GetComponent<Spline> (), "VerticalStraight", 1);
								} else if (i == 4) {
										enemyGroup.create ("tempCompassEnemy_v2", (spawnLocations [i]).transform, 6, (splinePaths [i]).GetComponent<Spline> (), "CyclicalLeft", -1);
								}
								enemyGroups.Enqueue (enemyGroup);
								++i;
						} else {
								counting = -1;
								print("There is no more spawning that needs to happen. just stop");		
						}
				} else {
			EnemyGroup_v2 enemyGroup = gameObject.AddComponent<EnemyGroup_v2> ();
			enemyGroup.create ("tempMagnetEnemy_v2", (spawnLocations [2]).transform, 6, (splinePaths [2]).GetComponent<Spline> (), "VerticalStraight", 1);
			enemyGroups.Enqueue (enemyGroup);
		}
	}
	
}
