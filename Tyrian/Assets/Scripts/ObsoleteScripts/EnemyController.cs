﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// This class is the head honcho. 
/// This controls what enemies are created, when, in what pattern, in what group, all that jazz
/// </summary>
public class EnemyController : MonoBehaviour {

	//later will have a collection of all enemies in here and then to create a new group of one, it will be pulled from the list. 
	public GameObject magnet;
	public GameObject wheatly;

	public GameObject[] spawnLocations = new GameObject[2];
	private Queue<EnemyData> enemySpawnList;

	public Camera cam;

	private void spawn(){
		/*new EnemyGroup(enemyData.getPatternType(), 
		               enemyData.getUnitMovement(), 
		               enemyData.getEnemyType(),
		               enemyData.getNumberOfEnemies(), 
		               enemyData.getSpawnOrigin(), 
		               enemyData.getEnemyToSpawn(), 
		               enemyData.getCam(),
		               enemyData.getDirection()
		               );*/
	}

	private void checkIfShouldspawn(){
		if (Input.GetKeyDown ("s")) {
			//spawn next enemy in queue
			//if (enemySpawnList.Count > 0) {
				//spawn(enemySpawnList.Dequeue());		
			spawn ();
			/*}else{
				print ("enemySpawnList is EMPTY! STOP PRESSING S");
			}*/
		}
	}

	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start () {

		/*enemySpawnList = new Queue<EnemyData> ();
		enemySpawnList.Enqueue (new EnemyData("VerticalStraight", 1, "magnet", 6, transform.FindChild ("MagnetSpawnLeft"), magnet, cam, "right"));
		enemySpawnList.Enqueue (new EnemyData("VerticalStraight", 1, "magnet", 6, transform.FindChild ("MagnetSpawnRight"), magnet, cam, "left"));
		enemySpawnList.Enqueue (new EnemyData("VerticalStraight", 1, "magnet", 6, transform.FindChild ("MagnetSpawnInnerLeft"), magnet, cam, "right"));
		enemySpawnList.Enqueue (new EnemyData("VerticalStraight", 1, "magnet", 6, transform.FindChild ("MagnetSpawnInnerRight"), magnet, cam, "left"));*/
	}

	/// <summary>
	/// Waits the please.
	/// </summary>
	/// <returns>The please.</returns>
	IEnumerator waitPlease()
	{
		float timeToWait = 0.25f;
		float incrementToRemove  = 0.25f;
		while(timeToWait > 0)
		{
			print ("timeToWait = " + timeToWait);
			yield return new WaitForSeconds(incrementToRemove );
			timeToWait -= incrementToRemove;
		}
		//spawnNextEnemy =1;
	}
	
	// Update is called once per frame
	void Update () {
		float time = Time.realtimeSinceStartup;
		print ("TIME = " +time);
		if (time > 5f) {
			spawn ();		
		}
		//checkIfShouldspawn ();
	}
}
