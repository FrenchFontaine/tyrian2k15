﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

	private int speed = 6;

	// Use this for initialization
	void Start () {
		GetComponent<Rigidbody2D>().velocity = new Vector2 (0, speed);
	}
	
	// Update is called once per frame
	void OnBecameInvisible () {
		Destroy (gameObject);
	}
}
